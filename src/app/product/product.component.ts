import { Component, OnInit } from '@angular/core';
import { Product } from '../common/interfaces/product';
import { ProductReference } from '../common/interfaces/product-reference';
import { ShoppingCartItem } from '../common/interfaces/shopping-cart-item';
import { ProductService } from './product.service';
import { ShoppingCartService } from "../common/shopping-cart/shopping-cart.service";
import { ActivatedRoute, ParamMap, Router, NavigationEnd } from '@angular/router';
import { FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../admin/auth/auth.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers: [ProductService, AuthService]
})
export class ProductComponent implements OnInit {

  product: Product;
  addedToCartRefences: ProductReference[];
  showCartDetailsLink: boolean;

  private initAddedToCartReferences(): void{
    this.addedToCartRefences = [new ProductReference()];
  }

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private service: ProductService,
    private shoppingCartService: ShoppingCartService,
    public authService: AuthService) { 
      this.initAddedToCartReferences();
      this.showCartDetailsLink = false;
  }  

  ngOnInit() {
    let productId = +this.route.snapshot.paramMap.get('id');

    let products: FirebaseListObservable<any>;
    products = this.service.getProduct(productId);
    products.subscribe( items => {
      this.product = items[0];
      console.log(this.product);
    });

    window.scrollTo(0, 0);
  }

  removeProductReferenceFromList(index: number): void{    
    this.addedToCartRefences.splice(index, 1);
  }
  
  getReferencePrice(title: string): number{
    return this.product.references.filter(item => item.title == title )[0].unitPrice;
  }

  addProductReferenceToList(): void{
    this.addedToCartRefences.push(new ProductReference());
  }

  updateReferencePrice(index: number, event: any): void{
    this.addedToCartRefences[index].title = event.target.value;
    this.addedToCartRefences[index].unitPrice = this.getReferencePrice(event.target.value);    
  }

  getThisProductSalePrice(): number{
    return this.addedToCartRefences.reduce(
      (accumulator,currentValue) => 
      {
        return accumulator + currentValue.getTotalPrice();
      }, 0);
  }

  getSalePriceIfThisAdded(): number{
    return this.getThisProductSalePrice() + this.shoppingCartService.getPaymentTotal();
  }

  getShoppingCartElementsCount(){
    return this.shoppingCartService.getElementsCount();
  }

  addToCart(): void{
    for (var index = 0; index < this.addedToCartRefences.length; index++) {
      let referece = this.addedToCartRefences[index];
      if (referece.quantity > 0) {
        let item = new ShoppingCartItem();
        item.productName = this.product.title;
        item.details = referece;
        this.shoppingCartService.addElements(item);
      }
    }

    this.initAddedToCartReferences();
    this.showCartDetailsLink = true;
  }
}
