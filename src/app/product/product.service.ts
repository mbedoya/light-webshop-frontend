import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Product } from '../common/interfaces/product';
import { ProductReference } from '../common/interfaces/product-reference';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProductService {

  private url = "https://jsonplaceholder.typicode.com/photos";

  constructor(private http: Http, private af: AngularFireDatabase) { }

  getProduct(id: number): FirebaseListObservable<any>{
    return this.af.list("products", {
      query: {
        orderByChild: "id",
        equalTo: id
      }
    });
  }

  getProducts(id: number): Promise<Product> {

     /*
    return this.http.get(this.url + "/" + id)
      .toPromise()
      .then(response => response.json());
 */
     
      var product = new Product();
      product.id = id;
      product.title = "Cábanos";
      product.description = `Es un Snack funcional blando de sabor a carne ahumada , elaborado con carne fresca, que brinda nuevas sensaciones en
      el paladar de la mascota. Formulado especialmente para perros de 3 meses en adelante.`;

      let reference1 = new ProductReference();
      reference1.title = "100 gr";
      reference1.unitPrice = 10000;

      let reference2 = new ProductReference();
      reference2.title = "200 gr";
      reference2.unitPrice = 15000;

      product.references = [reference1, reference2];

      return Promise.resolve(product);

  }
}