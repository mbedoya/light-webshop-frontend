import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ContentService {

  contents: FirebaseListObservable<any>;
  private selectedContent: any;

  constructor(private database: AngularFireDatabase) { 
    this.contents = this.database.list("/content");
  }
  
  getContents(): FirebaseListObservable<any>{
    return this.contents;
  }

  setSelectedContent(content: any){
    this.selectedContent = content;
  }

  getContentObserver(): Observable<any>{
    return new Observable(observer => {
      let prevContent = this.selectedContent;
      setInterval(() => {
        if (prevContent != this.selectedContent) {
          observer.next(this.selectedContent);
          prevContent = this.selectedContent;
        }
      }, 500);
      
    });
  }

  getSelectedContent(): any{
    return this.selectedContent;
  }
}
