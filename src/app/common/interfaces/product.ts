import { ProductReference } from './product-reference';

export class Product {
    id: number;
    title: string;
    imageUrl: string;
    featuredImageUrl: string;
    description: string;
    extendedDescription: string;
    url: string;
    references: ProductReference[];
    categories: string[];
    featured: boolean;

    constructor(){}
}
