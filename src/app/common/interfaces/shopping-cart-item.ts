import { ProductReference  } from "../interfaces/product-reference";

export class ShoppingCartItem {
    productName: string;
    details: ProductReference;
}