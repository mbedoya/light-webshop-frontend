export class ProductReference {
    title: string;
    quantity: number = 1;
    unitPrice: number = 0;

    constructor(){}

    getTotalPrice(): number{
        return this.quantity*this.unitPrice;
    }
}
