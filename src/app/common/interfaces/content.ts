export class Content{
    id: number;
    title: string;
    friendlyUrl: string;
    description: string;
    details: any[];
}