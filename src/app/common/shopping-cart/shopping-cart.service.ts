import { Injectable } from '@angular/core';
import { ProductReference } from "../interfaces/product-reference";
import { ShoppingCartItem } from "../interfaces/shopping-cart-item";

@Injectable()
export class ShoppingCartService {

  private items: ShoppingCartItem[];
  private paymentTotal: number;
  private shippingPrice: number;

  constructor() {
    this.items = [];
    this.shippingPrice = 4000;
  }

  getShippingPrice(): number{
    return this.shippingPrice;
  }

  getTotalProductosPrice(): number{
    return this.items.reduce(
      (accum, current) => {
        return Number(accum) + Number(current.details.getTotalPrice());
      }, 0 );
  }

  getPaymentTotal(): number{
    return this.shippingPrice + this.getTotalProductosPrice();
  }

  getElementsCount(): number{
    return this.items.reduce(
      (accum, current) => {
        return Number(accum) + Number(current.details.quantity);
      }, 0 );
  }

  getElements(): ShoppingCartItem[]{
    return this.items;
  }

  addElements(item: ShoppingCartItem): void{
    this.items.push(item);
  }

  removeElement(index: number): void{    
    this.items.splice(index, 1);
  }
}
