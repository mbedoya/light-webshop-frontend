import { TestBed, inject } from '@angular/core/testing';

import { FeaturedProductsService } from './featured-products.service';

describe('FeaturedProductsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeaturedProductsService]
    });
  });

  it('should be created', inject([FeaturedProductsService], (service: FeaturedProductsService) => {
    expect(service).toBeTruthy();
  }));
});
