import { Injectable } from '@angular/core';
import { Product } from '../interfaces/product';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FeaturedProductsFirebaseService {

  private collectionName: string;
  count: number;

  constructor(private af: AngularFireDatabase) { 
    this.collectionName = "products";

    this.getAllProducts().map( list => list.length ).subscribe( elem => this.count = elem );
  }

  getProducts(limitsTo: number): FirebaseListObservable<any> {
    return this.af.list(this.collectionName, {
        query: {
          orderByChild: 'featured',
          equalTo: true
        }
      }
    );
  }

  getAllProducts(): FirebaseListObservable<any> {
    return this.af.list(this.collectionName);
  }

  getProductReferenceByKey(databaseId: string): FirebaseObjectObservable<any>{
    return this.af.object(this.collectionName + '/' + databaseId);
  }

  updateProduct(databaseId: string, product: any): void{
    if (!product.categories || product.categories.length == 0) {
      product.categories = ["Snack Perritos"];
    }
    this.af.object(this.collectionName + '/' + databaseId).update(product);
  }

  addProduct(product: any): void{
    product.id = this.count + 1;
    if (!product.categories || product.categories.length == 0) {
      product.categories = ["Snack Perritos"];
    }
    this.af.list(this.collectionName).push(product).catch( error => { console.log(error); });
  }

}
