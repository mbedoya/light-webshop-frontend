import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Product } from '../interfaces/product';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class FeaturedProductsService {

  private url = "https://jsonplaceholder.typicode.com/photos";

  constructor(private http: Http) { }

  getProducts(): Promise<Product[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(response => response.json());
  }

  getAllProducts(): Promise<Product[]> {
    return this.http.get(this.url)
      .toPromise()
      .then(response => response.json());
  }
}
