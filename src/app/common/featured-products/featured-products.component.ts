import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2/database';
import { FeaturedProductsFirebaseService } from "./featured-products-firebase.service";
import { Product } from '../interfaces/product';

@Component({
  selector: 'app-featured-products',
  templateUrl: './featured-products.component.html',
  styleUrls: ['./featured-products.component.css'],
  providers: [ FeaturedProductsFirebaseService ]
})
export class FeaturedProductsComponent implements OnInit {

  products: FirebaseListObservable<any[]>;
  maxProductsToDisplay: number = 4;
  maxTitlePreviewSize: number = 20;
  maxDescriptionPreviewSize: number = 80;

  constructor(private service: FeaturedProductsFirebaseService) { }

  titlePreview(product: Product): string{
    if (product.title.length > this.maxTitlePreviewSize) {
      return product.title.substring(0, this.maxTitlePreviewSize) + " ...";
    }

    return product.title;
  }

  descriptionPreview(product: Product): string{
    if (product.description.length > this.maxDescriptionPreviewSize) {
      return product.description.substring(0, this.maxDescriptionPreviewSize) + " ...";
    }

    return product.description;
  }

  ngOnInit() {
    this.products = this.service.getProducts(this.maxProductsToDisplay);
  }
}
