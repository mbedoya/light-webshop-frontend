import { Injectable } from '@angular/core';

@Injectable()
export class LeftMenuService {

  constructor() { }

  getMenuItems(): string[]{
    return ["Snacks Perritos", "Cárnicos Perritos", "Snacks Gatos"];
  }

}
