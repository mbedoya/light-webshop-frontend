import { Component, OnInit } from '@angular/core';
import { LeftMenuService } from './left-menu.service';

@Component({
  selector: 'app-left-menu',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.css']
})
export class LeftMenuComponent implements OnInit {

  menuItems: string[];

  constructor() { }

  ngOnInit() {
    var service = new LeftMenuService();
    this.menuItems = service.getMenuItems();
  }

}
