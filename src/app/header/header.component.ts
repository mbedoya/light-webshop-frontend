import { Component } from "@angular/core";
import { ShoppingCartService } from "../common/shopping-cart/shopping-cart.service";
import { ContentService } from "../common/content/content.service";
import { FirebaseListObservable } from 'angularfire2/database';
import { AuthService } from '../admin/auth/auth.service'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    providers: [AuthService]
  })
export class HeaderComponent{
    headerMenuActive: boolean;
    contents: FirebaseListObservable<any>;    

    constructor(private shoppingCartService: ShoppingCartService, 
      private contentService: ContentService,
      public authService: AuthService){
      this.headerMenuActive = true;
      this.contents = contentService.getContents();
    }

    toggleHeaderMenu(): void{
      this.headerMenuActive = !this.headerMenuActive;
    }

    getElementsInCartCount(): number{
      return this.shoppingCartService.getElementsCount();
    }

    onSelectContent(content: any){
      this.contentService.setSelectedContent(content);
    }

    onSelectWhereToBuy(){
      this.contentService.setSelectedContent({ id: 2, title: '¿Dónde comprar?', description:'Encuéntranos en Tierragro' });
    }
}