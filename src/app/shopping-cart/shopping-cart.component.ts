import { Component, OnInit } from '@angular/core';
import { ShoppingCartItem } from "../common/interfaces/shopping-cart-item";
import { ShoppingCartService } from "../common/shopping-cart/shopping-cart.service";

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  constructor(private shoppingCartService: ShoppingCartService) { }

  ngOnInit() {

  }

  getElementsCount(): number{
    return this.shoppingCartService.getElementsCount();
  }

  getElements(): ShoppingCartItem[]{
    return this.shoppingCartService.getElements();
  }

  getTotal(): number{
    return this.shoppingCartService.getPaymentTotal();
  }

  removeElement(index: number): void{
    this.shoppingCartService.removeElement(index);
  }
}
