import { Component, OnInit } from '@angular/core';
import { ContentService } from "../common/content/content.service";
import { FirebaseListObservable } from 'angularfire2/database';
import { Content } from '../common/interfaces/content';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  content: Content;

  constructor(private contentService: ContentService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.content = this.contentService.getSelectedContent();

    const content$ = this.contentService.getContentObserver();
    content$.subscribe(c => {
      this.content = c;
    });
  }

}
