import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2/database';
import { FeaturedProductsFirebaseService } from "../common/featured-products/featured-products-firebase.service";
import { Product } from '../common/interfaces/product';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ FeaturedProductsFirebaseService ]
})
export class ProductsComponent implements OnInit {

  products: FirebaseListObservable<any[]>;
  productsToDisplay: Observable<any[]>;
  maxTitlePreviewSize: number = 20;
  maxDescriptionPreviewSize: number = 80;
  sub: any;

  constructor(private service: FeaturedProductsFirebaseService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.products = this.service.getAllProducts();

    this.sub = this.route.params.subscribe(params => {
      
      let category = params['category'];
      console.log(category);

      if(category != "all"){
        this.productsToDisplay = this.products
        .map ( cats => cats.filter(x => 
                x.categories && x.categories.filter( cat => cat == category).length > 0
             )
        );

      }else{
        this.productsToDisplay = this.products;
      }

   });
  }

  titlePreview(product: Product): string{
    if (product.title.length > this.maxTitlePreviewSize) {
      return product.title.substring(0, this.maxTitlePreviewSize) + " ...";
    }

    return product.title;
  }

  descriptionPreview(product: Product): string{
    if (product.description.length > this.maxDescriptionPreviewSize) {
      return product.description.substring(0, this.maxDescriptionPreviewSize) + " ...";
    }

    return product.description;
  }

}
