import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';
import { AngularFireModule } from "angularfire2";
import { AngularFireDatabaseModule } from 'angularfire2/database'
import { AngularFireAuthModule } from "angularfire2/auth";
import { environment } from '../environments/environment'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component'
import { LeftMenuComponent } from './common/left-menu/left-menu.component';
import { FeaturedProductsComponent } from './common/featured-products/featured-products.component';
import { CarrouselComponent } from './common/carrousel/carrousel.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ProductComponent } from './product/product.component';
import { ContentService } from './common/content/content.service';
import { ShoppingCartService } from './common/shopping-cart/shopping-cart.service';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ContentComponent } from './content/content.component';
import { ProductsComponent } from './products/products.component';
import { ManageProductsComponent } from './admin/manage-products/manage-products.component';
import { ManageProductComponent } from './admin/manage-product/manage-product.component';

@NgModule({
  declarations: [
    AppComponent, HeaderComponent, 
    LeftMenuComponent, FeaturedProductsComponent, 
    CarrouselComponent, HomeComponent, FooterComponent, ProductComponent, ShoppingCartComponent, ContentComponent, ProductsComponent, ManageProductsComponent, ManageProductComponent
  ],
  imports: [
    BrowserModule, HttpModule, FormsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },{
        path: 'products/:category',
        component: ProductsComponent
      },{
        path: 'product/:id',
        component: ProductComponent
      },{
        path: 'shopping-cart',
        component: ShoppingCartComponent
      },{
        path: 'content/:id',
        component: ContentComponent,
      },{
        path: 'admin',
        component: ManageProductsComponent,
      },{
        path: 'manage-product/:id',
        component: ManageProductComponent,
      }      
    ]),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [ShoppingCartService, ContentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
