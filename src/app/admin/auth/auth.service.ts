import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthService {

  user: Observable<firebase.User>;

  constructor(private firebaseAuth: AngularFireAuth) {
      this.user = firebaseAuth.authState;
   }

  loginWithCredentials(email: string, password: string){
    let provider = new firebase.auth.EmailAuthProvider();
    this.firebaseAuth.auth.signInWithEmailAndPassword( email , password )
    .then( result => { console.log(result) })
    .catch( error => { console.log(error) });
  }

  logout(){
    this.firebaseAuth.auth.signOut();
  }

}
