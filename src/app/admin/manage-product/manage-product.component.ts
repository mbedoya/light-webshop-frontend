import { Component, OnInit } from '@angular/core';
import { Product } from '../../common/interfaces/product';
import { ProductReference } from '../../common/interfaces/product-reference';
import { FirebaseObjectObservable } from 'angularfire2/database';
import { FeaturedProductsFirebaseService } from "../../common/featured-products/featured-products-firebase.service";
import { ActivatedRoute } from '@angular/router';
import { LeftMenuService } from '../../common/left-menu/left-menu.service';

@Component({
  selector: 'app-manage-product',
  templateUrl: './manage-product.component.html',
  styleUrls: ['./manage-product.component.css'],
  providers: [FeaturedProductsFirebaseService, LeftMenuService]
})
export class ManageProductComponent implements OnInit {

  model: any;
  sub: any;
  databaseProductId: string;
  displaySavedMessage: boolean = false;
  availableCategories: any[];

  constructor(private dataService: FeaturedProductsFirebaseService, 
    private route: ActivatedRoute, 
    private leftMenuService: LeftMenuService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {

      this.databaseProductId = params['id'];

      if (this.databaseProductId != "0") {
        let productObserver = this.dataService.getProductReferenceByKey(this.databaseProductId);
        productObserver.subscribe(element => { this.updateProductInfo(element) });
      } else {
        this.updateProductInfo(new Product());
      }

    });

  }

  private updateProductInfo(product: any){
    this.model = product;
    this.mapAllCategoriesToSelectedCategories();
  }

  onToggleCategory(index: number){
    console.log("changing category selection");
    this.availableCategories[index].checked = !this.availableCategories[index].checked;
  }

  private mapAllCategoriesToSelectedCategories(){

    let allCategories = this.leftMenuService.getMenuItems();
    this.availableCategories = [];

    for (let index = 0; index < allCategories.length; index++) {

      let productHasCategory: boolean = false;
      if (this.model.categories && this.model.categories.filter( e => e == allCategories[index]).length > 0) {
        productHasCategory = true;
      }

      console.log(this.model);
      console.log(allCategories[index]);
      console.log(productHasCategory);

      this.availableCategories.push( {text: allCategories[index], value: index, checked: productHasCategory });
    }
  }

  onSaveProduct() {

    this.model.categories = this.availableCategories.filter(c => c.checked).map( e => e = e.text);

    console.log(this.availableCategories);
    console.log(this.model.categories);

    if (this.databaseProductId != "0") {
      this.dataService.updateProduct(this.databaseProductId, this.model);
    }else{
      this.dataService.addProduct(this.model);
    }
    this.displaySavedMessage = true;

  }

}
