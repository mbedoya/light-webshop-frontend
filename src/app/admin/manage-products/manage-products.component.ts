import { Component, OnInit } from '@angular/core';
import { FirebaseListObservable } from 'angularfire2/database';
import { FeaturedProductsFirebaseService } from "../../common/featured-products/featured-products-firebase.service";
import { Product } from '../../common/interfaces/product';
import { AuthService } from '../auth/auth.service'

@Component({
  selector: 'app-manage-products',
  templateUrl: './manage-products.component.html',
  styleUrls: ['./manage-products.component.css'],
  providers: [ FeaturedProductsFirebaseService, AuthService ]
})
export class ManageProductsComponent implements OnInit {

  products: FirebaseListObservable<any[]>;
  maxTitlePreviewSize: number = 20;
  maxDescriptionPreviewSize: number = 80;

  private email: string;
  private password: string;

  constructor(private dataService: FeaturedProductsFirebaseService, public authService: AuthService) { }

  ngOnInit() {
    this.products = this.dataService.getAllProducts();
  }

  onLogin(){
    this.authService.loginWithCredentials(this.email, this.password);
  }

  onLogout(){
    this.authService.logout();
  }

  titlePreview(product: Product): string{
    if (product.title.length > this.maxTitlePreviewSize) {
      return product.title.substring(0, this.maxTitlePreviewSize) + " ...";
    }

    return product.title;
  }

  descriptionPreview(product: Product): string{
    if (product.description.length > this.maxDescriptionPreviewSize) {
      return product.description.substring(0, this.maxDescriptionPreviewSize) + " ...";
    }

    return product.description;
  }

}
